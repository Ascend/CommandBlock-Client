
# CommandBlock Client (CBC)
A fabric mod-pack hacked-client

<h3><span style="background-color: #FDE74C; color: #000000;">This project is just a proof of concept and is no longer maintained. The latest supported Minecraft version is 1.19</span></h3>

## Screenshots
Main Menu:
![enter image description here](https://gitea.com/Ascend/CommandBlock-Client/raw/branch/main/git-screenshots/Screenshot_20230723_110713.png)

HUD:
![enter image description here](https://gitea.com/Ascend/CommandBlock-Client/raw/branch/main/git-screenshots/Screenshot_20230723_111819.png)

Inventory:
![enter image description here](https://gitea.com/Ascend/CommandBlock-Client/raw/branch/main/git-screenshots/Screenshot_20230723_111827.png)

This is the mods + configuration that I use when playing on anarchy servers. I shared it with my friends and they say that I should publish it, so here I am!

It is a Frankenstein monstrosity put together with shit tons of mod and only look half decent. However it works for me.

## Features

| Features                   | How to use                                                   |
| -------------------------- | ------------------------------------------------------------ |
| OptiFine zoom              | Key bind: C                                                  |
| Mini-map + World map       | Key bind: M                                                  |
| Baritone                   | ".b" command in chat                                         |
| Keystrokes                 | To configure, key bind: Right Alt                            |
| Multi-connect/Via versions | Click on the top left button at multiplayer menu             |
| Proxy                      | Click on the "Proxies" button in the multiplayer menu        |
| Alt-Manager                | Click on the account button in the multiplayer menu          |
| NoChatReports              | Already configured and blocking chat reports                 |
| Music                      | Press shift to go into MatHax menu, go to the music tab using the bar on the top of the screen |
| MatHax Client              | Key bind: Right Shift                                        |
| BleachHax Client           | Key bind: Right Control                                      

## How to Install

First clone the repo by clicking on the "..." button beside the git URL, and click on "Download ZIP"
After you download the Zip extract to a folder.

Alternatively, if you have Git installed, you can run:

```bash
cd Downloads
git clone https://github.com/ImGGAAVVIINN/CommandBlock-Client.git
```

When you got all the files in a folder, then set the Minecraft directory to that folder. Finally launch the version "CBC 1.19"